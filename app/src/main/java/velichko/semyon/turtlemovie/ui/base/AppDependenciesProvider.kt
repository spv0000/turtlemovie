package velichko.semyon.turtlemovie.ui.base

import android.content.Context
import velichko.semyon.turtlemovie.ui.base.resource.ResourceProvider
import velichko.semyon.turtlemovie.ui.base.resource.ResourceProviderImpl

object AppDependenciesProvider {

    lateinit var resourceProvider: ResourceProvider

    fun init(context: Context) {
        resourceProvider = ResourceProviderImpl(context)
    }
}