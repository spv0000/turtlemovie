package velichko.semyon.turtlemovie.ui.base.adapter

interface DiffListItemModel {

    /**
     * Unique id that can be used to distinguish this item after dynamic data is changed.
     * id must be static, e.g. item id from BE, title for static headers.
     *
     * When DiffList is changed, it will find item by id, check its content and play change animation.
     * If id is changed, it will not find item and will play remove + add animation. This can also lead to errors.
     */
    val id: Any

    /**
     * Determines if this and [other] represents the same item in list.
     * If items have unique ids, isSame must compare them.
     */
    fun isSameAs(other: DiffListItemModel): Boolean =
        this::class.java == other::class.java && this.id == other.id

    /**
     * Determines if this and [other] contents are the same (i.e. if VH requires rebinding to this new item).
     * Only called when this.isSameAs(other) == true.
     * Use [RebindableDiffListItemModel] for views with listener (including two-way binding) so VH rebinds every time.
     */
    fun areContentsTheSame(other: DiffListItemModel): Boolean = this == other
}

interface RebindableDiffListItemModel : DiffListItemModel {
    override fun areContentsTheSame(other: DiffListItemModel): Boolean = this === other
}

@Deprecated(
    message = "This implementation is broken and only for back compatibility, use DiffListItemModel",
    replaceWith = ReplaceWith("DiffListItemModel")
)
interface DiffListItemModelCompat : DiffListItemModel {

    override val id: Any
        get() = javaClass
}