package velichko.semyon.turtlemovie.ui.movie.list.event

import androidx.fragment.app.Fragment
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.event.ViewEvent
import velichko.semyon.turtlemovie.ui.movie.comments.MovieCommentsActivity

class ViewEventOpenComments(private val movie: Movie) : ViewEvent {
    override fun execute(fragment: Fragment) {
        fragment.requireActivity().startActivity(MovieCommentsActivity.getIntent(fragment.requireContext(), movie))
    }
}