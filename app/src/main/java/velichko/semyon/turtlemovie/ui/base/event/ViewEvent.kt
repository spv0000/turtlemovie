package velichko.semyon.turtlemovie.ui.base.event

import androidx.fragment.app.Fragment

interface ViewEvent {
    fun execute(fragment: Fragment)
}