package velichko.semyon.turtlemovie.ui.base.event

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.BaseTransientBottomBar
import velichko.semyon.turtlemovie.ui.base.snack.SnackHelper

class ViewEventShowActionSnack(
    private val message: String,
    private val actionBtnText: String,
    private val onBtnClickAction: () -> Unit,
    private val length: Int = BaseTransientBottomBar.LENGTH_INDEFINITE
) : ViewEvent {

    override fun execute(fragment: Fragment) = SnackHelper.show(fragment) {
        setText(message)
        setAction(actionBtnText) { onBtnClickAction.invoke() }
        duration = length
    }
}