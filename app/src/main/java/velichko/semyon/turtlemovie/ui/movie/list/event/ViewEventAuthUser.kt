package velichko.semyon.turtlemovie.ui.movie.list.event

import androidx.fragment.app.Fragment
import com.firebase.ui.auth.AuthUI
import velichko.semyon.turtlemovie.ui.base.event.ViewEvent
import velichko.semyon.turtlemovie.ui.movie.list.MovieListFragment.Companion.RC_SIGN_IN

class ViewEventAuthUser : ViewEvent {
    override fun execute(fragment: Fragment) {
        val intent = AuthUI.getInstance()
            .createSignInIntentBuilder()
            .setIsSmartLockEnabled(false)
            .setAvailableProviders(
                arrayListOf(
                    AuthUI.IdpConfig.EmailBuilder().build(),
                    AuthUI.IdpConfig.GoogleBuilder().build()
                )
            )
            .build()

        fragment.requireActivity().startActivityForResult(intent, RC_SIGN_IN)
    }
}