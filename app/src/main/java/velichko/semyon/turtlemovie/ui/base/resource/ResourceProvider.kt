package velichko.semyon.turtlemovie.ui.base.resource

import android.content.Context
import androidx.annotation.ArrayRes
import androidx.annotation.StringRes

interface ResourceProvider {
    fun getString(@StringRes stringResId: Int): String
    fun getStringArray(@ArrayRes stringArrayResId: Int): List<String>
}

internal class ResourceProviderImpl(private val context: Context) : ResourceProvider {
    override fun getString(stringResId: Int): String {
        return context.getString(stringResId)
    }

    override fun getStringArray(stringArrayResId: Int): List<String> {
        return context.resources.getStringArray(stringArrayResId).toList()
    }
}