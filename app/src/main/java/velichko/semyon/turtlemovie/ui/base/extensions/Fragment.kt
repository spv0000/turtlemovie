package velichko.semyon.turtlemovie.ui.base.extensions

import android.os.Bundle
import androidx.fragment.app.Fragment

inline fun <T : Fragment> T.applyArgs(crossinline block: Bundle.() -> Unit): T {
    arguments = (arguments ?: Bundle()).apply(block)
    return this
}