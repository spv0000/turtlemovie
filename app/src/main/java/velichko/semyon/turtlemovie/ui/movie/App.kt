package velichko.semyon.turtlemovie.ui.movie

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import velichko.semyon.turtlemovie.ui.base.AppDependenciesProvider

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppDependenciesProvider.init(this)
    }
}