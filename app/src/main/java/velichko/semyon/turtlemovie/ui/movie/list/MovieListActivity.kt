package velichko.semyon.turtlemovie.ui.movie.list

import velichko.semyon.turtlemovie.ui.base.BaseActivity

class MovieListActivity : BaseActivity() {

    override fun createInitialFragment() = MovieListFragment()
}