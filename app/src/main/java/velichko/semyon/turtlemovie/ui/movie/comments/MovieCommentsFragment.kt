package velichko.semyon.turtlemovie.ui.movie.comments

import velichko.semyon.turtlemovie.BR
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.BaseFragment
import velichko.semyon.turtlemovie.ui.base.extensions.applyArgs
import velichko.semyon.turtlemovie.ui.movie.comments.MovieCommentsActivity.Companion.EXTRA_MOVIE
import velichko.semyon.turtlemovie.ui.movie.comments.vm.MovieCommentsViewModel
import velichko.semyon.turtlemovie.ui.movie.comments.vm.MovieCommentsViewModelFactory

class MovieCommentsFragment : BaseFragment<MovieCommentsViewModel>(MovieCommentsViewModel::class.java) {

    override val layoutId = R.layout.fragment_movie_comments

    override val dataBindingVariable = BR.vmMovieComments

    override fun getViewModelFactory() = MovieCommentsViewModelFactory(arguments?.getParcelable(EXTRA_MOVIE)!!)

    companion object {
        fun getFragment(movie: Movie) = MovieCommentsFragment().applyArgs {
            putParcelable(EXTRA_MOVIE, movie)
        }
    }
}