package velichko.semyon.turtlemovie.ui.base.extensions

import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.BindingAdapter

fun View.showKeyboard() {
    context ?: return
    requestFocus()
    context.inputMethodManager.showSoftInput(currentFocus, InputMethodManager.SHOW_IMPLICIT)
}

private val View.currentFocus: View?
    get() {
        if (isFocused) return this
        return (this as? ViewGroup)?.findFocus()
    }

@BindingAdapter("visible")
fun View.setVisible(visible: Boolean) = setVisible(visible, true)

@BindingAdapter("visible", "visibleHideWithGone", requireAll = true)
fun View.setVisible(visible: Boolean, visibleHideWithGone: Boolean) {

    visibility = when {

        visible -> View.VISIBLE
        visibleHideWithGone -> View.GONE
        else -> View.INVISIBLE
    }
}