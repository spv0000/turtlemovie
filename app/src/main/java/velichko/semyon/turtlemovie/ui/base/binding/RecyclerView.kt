package velichko.semyon.turtlemovie.ui.base.binding

import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.ui.base.adapter.DiffListAdapter
import velichko.semyon.turtlemovie.ui.base.adapter.DiffListItemModel

@BindingAdapter("adapter", "fixedSize", "needDivider", requireAll = false)
fun <T : DiffListItemModel> setRecyclerViewAdapterAndFixedSize(
    rv: RecyclerView,
    adapter: DiffListAdapter<T>? = null,
    fixedSize: Boolean? = null,
    needDivider: Boolean = false
) {
    rv.adapter = adapter
    setFixedSize(rv, fixedSize)
    if (needDivider) {
        val dividerItemDecoration = DividerItemDecoration(rv.context, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(ColorDrawable(ContextCompat.getColor(rv.context, R.color.divider)))
        rv.addItemDecoration(dividerItemDecoration)
    }
}

@BindingAdapter("fixedSize")
fun setFixedSize(rv: RecyclerView, fixedSize: Boolean?) {
    fixedSize ?: return
    rv.setHasFixedSize(fixedSize)
}

@BindingAdapter("scrollToPosition")
fun scrollToPosition(rv: RecyclerView, position: Int) {
    val layoutManager: LinearLayoutManager = rv.layoutManager as? LinearLayoutManager ?: return
    rv.post {
        val lastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition()

        if (position > lastCompletelyVisibleItemPosition) {
            val smoothScroller: RecyclerView.SmoothScroller = object : LinearSmoothScroller(rv.context) {
                override fun getVerticalSnapPreference(): Int = SNAP_TO_START
            }
            smoothScroller.targetPosition = position
            layoutManager.startSmoothScroll(smoothScroller)
        }
    }
}
