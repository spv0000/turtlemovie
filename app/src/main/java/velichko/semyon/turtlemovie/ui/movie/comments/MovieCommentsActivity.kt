package velichko.semyon.turtlemovie.ui.movie.comments

import android.content.Context
import android.content.Intent
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.BaseActivity

class MovieCommentsActivity : BaseActivity() {

    override fun createInitialFragment() = MovieCommentsFragment.getFragment(intent.getParcelableExtra(EXTRA_MOVIE)!!)

    companion object {
        const val EXTRA_MOVIE = "extra_movie"

        fun getIntent(context: Context, movie: Movie) = Intent(context, MovieCommentsActivity::class.java).also {
            it.putExtra(EXTRA_MOVIE, movie)
        }
    }
}