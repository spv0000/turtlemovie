package velichko.semyon.turtlemovie.ui.base.event

import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import velichko.semyon.turtlemovie.ui.base.extensions.showKeyboard

class ViewEventRequestFocus(
    @IdRes private val viewId: Int,
    private val showKeyboard: Boolean = true
) : ViewEvent {

    override fun execute(fragment: Fragment) {
        val view = fragment.view!!.findViewById<View>(viewId) ?: return
        if (showKeyboard) GlobalScope.launch(Main) {
            delay(KEYBOARD_SHOW_AFTER_FOCUS_DELAY_MILLIS)
            view.showKeyboard()
        } else {
            view.requestFocus()
        }
    }
}

private const val KEYBOARD_SHOW_AFTER_FOCUS_DELAY_MILLIS = 150L