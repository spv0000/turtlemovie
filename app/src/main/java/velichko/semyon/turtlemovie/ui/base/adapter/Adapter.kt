package velichko.semyon.turtlemovie.ui.base.adapter

import androidx.recyclerview.widget.RecyclerView

interface Adapter<T> {

    val itemList: List<T>
    fun getItemCount(): Int
    fun submitList(newList: List<T>)
    fun setListener(listener: Listener)

    interface Listener {

        fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>?) = Unit
    }
}