package velichko.semyon.turtlemovie.ui.movie.list.vm

import velichko.semyon.turtlemovie.data.MovieListRepository
import velichko.semyon.turtlemovie.ui.base.AppDependenciesProvider
import velichko.semyon.turtlemovie.ui.base.BaseViewModelFactory

class MovieListViewModelFactory : BaseViewModelFactory<MovieListViewModel>() {

    override fun getViewModel() = MovieListViewModel(
        movieListRepository = MovieListRepository(),
        resourceProvider = AppDependenciesProvider.resourceProvider
    )
}