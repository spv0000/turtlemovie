package velichko.semyon.turtlemovie.ui.base.event

import android.widget.Toast
import androidx.fragment.app.Fragment
import velichko.semyon.turtlemovie.ui.base.extensions.showToast

sealed class ViewEventShowToast(private val text: String) : ViewEvent {

    override fun execute(fragment: Fragment) {
        val duration = when (this) {
            is Long -> Toast.LENGTH_LONG
            is Short -> Toast.LENGTH_SHORT
        }

        fragment.context?.showToast(text, duration)
    }

    class Long(text: String) : ViewEventShowToast(text)
    class Short(text: String) : ViewEventShowToast(text)
}