package velichko.semyon.turtlemovie.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

abstract class BaseFragment<T : BaseViewModel>(private val viewModelClass: Class<T>) : Fragment() {

    protected abstract val dataBindingVariable: Int

    @get:LayoutRes
    protected abstract val layoutId: Int

    protected val viewModel: T
        get() = _viewModel

    private lateinit var _viewModel: T

    private lateinit var binding: ViewDataBinding

    private val _viewModelFactory: BaseViewModelFactory<T> by lazy { getViewModelFactory() }

    private val onBackPressedCallback by lazy {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() = onBackPressed()
        }
    }

    protected abstract fun getViewModelFactory(): BaseViewModelFactory<T>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.let(this::restoreState)
        initViews(savedInstanceState)
    }

    protected open fun initViews(savedInstanceState: Bundle?) {
        initViewModel()
        subscribeBaseViewModelEvents()
        lifecycle.addObserver(viewModel)
    }

    final override fun onSaveInstanceState(outState: Bundle) {
        val bundle = saveState(outState)
        super.onSaveInstanceState(bundle)
    }

    @CallSuper
    protected open fun saveState(bundle: Bundle): Bundle = bundle

    @CallSuper
    protected open fun restoreState(bundle: Bundle) = Unit

    protected open fun onBackPressed() {
        viewModel.onBackPressed()
    }

    private fun initViewModel() {
        _viewModel = ViewModelProviders.of(this, _viewModelFactory).get(viewModelClass)
        binding.setVariable(dataBindingVariable, viewModel)
        binding.lifecycleOwner = this
    }

    private fun subscribeBaseViewModelEvents() {
        observeNotNull(viewModel.events) { event -> event.execute(this) }
    }
}