package velichko.semyon.turtlemovie.ui.movie.list.vm

import android.app.Activity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import velichko.semyon.turtlemovie.BR
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.data.MovieListRepository
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.BaseViewModel
import velichko.semyon.turtlemovie.ui.base.adapter.SingleItemLayoutDiffListAdapter
import velichko.semyon.turtlemovie.ui.base.event.ViewEventShowActionSnack
import velichko.semyon.turtlemovie.ui.base.event.ViewEventShowToast
import velichko.semyon.turtlemovie.ui.base.resource.ResourceProvider
import velichko.semyon.turtlemovie.ui.movie.list.event.ViewEventAuthUser
import velichko.semyon.turtlemovie.ui.movie.list.event.ViewEventOpenComments
import velichko.semyon.turtlemovie.ui.movie.list.model.MovieListItemView

class MovieListViewModel(
    private val movieListRepository: MovieListRepository,
    private val resourceProvider: ResourceProvider
) : BaseViewModel() {

    val adapter = SingleItemLayoutDiffListAdapter<MovieListItemView>(
        itemLayoutId = R.layout.li_movie,
        dataBindingVariable = BR.vmMovie
    )

    val isLoading = MutableLiveData(false)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        when (Firebase.auth.currentUser == null) {
            true -> loginUser()
            false -> if (adapter.itemList.isEmpty()) getMovieList()
        }
    }

    private fun loginUser() {
        postViewEvents(ViewEventAuthUser())
    }

    private fun getMovieList() {
        launch {
            isLoading.value = true
            movieListRepository.getMovieList(
                successAction = { items ->
                    val onClickAction: (Movie) -> Unit = { movie -> postViewEvents(ViewEventOpenComments(movie)) }
                    adapter.submitList(items.map { MovieListItemView(it, onClickAction) })
                    isLoading.value = false
                },
                errorAction = {
                    postViewEvents(ViewEventShowActionSnack(
                        message = resourceProvider.getString(R.string.error_common),
                        actionBtnText = resourceProvider.getString(R.string.retry),
                        onBtnClickAction = { getMovieList() }
                    ))
                    isLoading.value = false
                }
            )
        }
    }

    fun handleActivityResult(resultCode: Int) {

        when (resultCode) {
            Activity.RESULT_OK -> {
                postViewEvents(ViewEventShowToast.Long("RESULT_OK"))
                getMovieList()
            }
            else -> {
                val errorMessage = resourceProvider.getString(R.string.error_auth)
                postViewEvents(
                    ViewEventShowToast.Long(errorMessage),
                    ViewEventAuthUser()
                )
            }
        }
    }

    fun logout() {
        Firebase.auth.signOut()
        loginUser()
    }
}