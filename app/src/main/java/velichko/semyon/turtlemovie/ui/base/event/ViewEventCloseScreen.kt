package velichko.semyon.turtlemovie.ui.base.event

import androidx.fragment.app.Fragment

class ViewEventCloseScreen : ViewEvent {
    override fun execute(fragment: Fragment) {
        val supportFragmentManager = fragment.activity?.supportFragmentManager ?: return
        when (supportFragmentManager.fragments.size > 1) {
            true -> supportFragmentManager.popBackStackImmediate()
            else -> fragment.activity?.finish()
        }
    }
}