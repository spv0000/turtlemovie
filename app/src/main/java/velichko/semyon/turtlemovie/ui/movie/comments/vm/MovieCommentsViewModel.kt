package velichko.semyon.turtlemovie.ui.movie.comments.vm

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.launch
import velichko.semyon.turtlemovie.BR
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.data.MovieListRepository
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.BaseViewModel
import velichko.semyon.turtlemovie.ui.base.adapter.SingleItemLayoutDiffListAdapter
import velichko.semyon.turtlemovie.ui.base.event.ViewEventCloseScreen
import velichko.semyon.turtlemovie.ui.base.event.ViewEventRequestFocus
import velichko.semyon.turtlemovie.ui.base.event.ViewEventShowToast
import velichko.semyon.turtlemovie.ui.base.resource.ResourceProvider
import velichko.semyon.turtlemovie.ui.movie.comments.model.MovieCommentListItemView


class MovieCommentsViewModel(
    val movie: Movie,
    private val resourceProvider: ResourceProvider,
    private val movieListRepository: MovieListRepository
) : BaseViewModel() {

    val adapter = SingleItemLayoutDiffListAdapter<MovieCommentListItemView>(
        itemLayoutId = R.layout.li_movie_comment,
        dataBindingVariable = BR.vmMovieComment
    )

    val comment = MutableLiveData("")
    val isEmptyViewVisible = MutableLiveData(false)
    val scrollPosition = MutableLiveData(0)

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        postViewEvents(ViewEventRequestFocus(R.id.fragment_movie_comments_input))
        launch {
            movieListRepository.getMovie(movie) { movie ->
                if (movie.commentList != null) {
                    adapter.submitList(movie.commentList!!.map { MovieCommentListItemView(it) })
                    isEmptyViewVisible.value = adapter.itemCount == 0
                    scrollPosition.value = adapter.itemCount
                }
            }
        }
    }

    fun sendComment() {
        if (comment.value.isNullOrEmpty()) {
            postViewEvents(ViewEventShowToast.Long(resourceProvider.getString(R.string.empty_comment_message)))
            return
        }

        launch {
            movieListRepository.addComment(movie, comment.value!!)
            comment.value = ""
        }
    }

    fun closeScreen() {
        postViewEvents(ViewEventCloseScreen())
    }
}