package velichko.semyon.turtlemovie.ui.base.adapter

import androidx.annotation.LayoutRes

class SingleItemLayoutDiffListAdapter<T : DiffListItemModel>(

    @LayoutRes private val itemLayoutId: Int,
    private val dataBindingVariable: Int

) : DiffListAdapter<T>() {

    override fun getItemLayoutId(type: Int) = itemLayoutId

    override fun fillViewHolder(holder: BaseViewHolder?, item: T, viewType: Int, payloads: MutableList<Any>?) {

        holder?.binding?.setVariable(dataBindingVariable, item)
    }
}