package velichko.semyon.turtlemovie.ui.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class DiffListAdapter<T : DiffListItemModel> : RecyclerView.Adapter<BaseViewHolder>(), Adapter<T> {

    final override var itemList: List<T> = listOf()
        private set(value) {
            field = value.toList()
        }

    private val diffCallback = DiffCallbackImpl()

    private var callbackListener: Adapter.Listener? = null

    protected abstract fun getItemLayoutId(type: Int): Int

    protected abstract fun fillViewHolder(
        holder: BaseViewHolder?,
        item: T,
        viewType: Int,
        payloads: MutableList<Any>? = null
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater!!, getItemLayoutId(viewType), parent, false)
        val viewHolder = BaseViewHolder(binding)
        binding.lifecycleOwner = viewHolder
        return viewHolder
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        fillViewHolder(holder, getItem(position), getItemViewType(position))
        callbackListener?.onBindViewHolder(holder, position, null)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int, payloads: MutableList<Any>) {
        fillViewHolder(holder, getItem(position), getItemViewType(position), payloads)
        callbackListener?.onBindViewHolder(holder, position, payloads)
    }

    override fun onViewAttachedToWindow(holder: BaseViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAttached()
    }

    override fun onViewDetachedFromWindow(holder: BaseViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDetached()
    }

    final override fun setListener(listener: Adapter.Listener) {
        this.callbackListener = listener
    }

    override fun submitList(newList: List<T>) {

        diffCallback.setLists(oldList = itemList, newList = newList)
        itemList = newList
        DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
    }

    fun setListWithoutUpdates(list: List<T>) {
        itemList = list
    }

    final override fun getItemCount() = itemList.size

    fun getItem(position: Int): T = itemList[position]

    @Deprecated(
        message = "use itemList property",
        replaceWith = ReplaceWith("itemList")
    )
    fun getItems(): List<T> = itemList
}

private class DiffCallbackImpl : DiffUtil.Callback() {

    private var oldList: List<DiffListItemModel> = listOf()
    private var newList: List<DiffListItemModel> = listOf()

    fun setLists(oldList: List<DiffListItemModel>, newList: List<DiffListItemModel>) {
        this.oldList = oldList.toList()
        this.newList = newList.toList()
    }

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPos: Int, newItemPos: Int) =
        oldList[oldItemPos].isSameAs(newList[newItemPos])

    override fun areContentsTheSame(oldItemPos: Int, newItemPos: Int) =
        oldList[oldItemPos].areContentsTheSame(newList[newItemPos])
}

class BaseViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root), LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    init {
        lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
    }

    fun onAttached() {
        lifecycleRegistry.currentState = Lifecycle.State.STARTED
    }

    fun onDetached() {
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED
    }

    override fun getLifecycle(): Lifecycle = lifecycleRegistry
}