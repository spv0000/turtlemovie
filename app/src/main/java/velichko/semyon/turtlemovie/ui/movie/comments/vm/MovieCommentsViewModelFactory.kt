package velichko.semyon.turtlemovie.ui.movie.comments.vm

import velichko.semyon.turtlemovie.data.MovieListRepository
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.AppDependenciesProvider
import velichko.semyon.turtlemovie.ui.base.BaseViewModelFactory


class MovieCommentsViewModelFactory(private val movie: Movie) : BaseViewModelFactory<MovieCommentsViewModel>() {

    override fun getViewModel() = MovieCommentsViewModel(
        movie = movie,
        movieListRepository = MovieListRepository(),
        resourceProvider = AppDependenciesProvider.resourceProvider
    )
}