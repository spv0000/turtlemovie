package velichko.semyon.turtlemovie.ui.base.snack

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

object SnackHelper {

    private var snackHolder: SnackHolder? = null

    fun show(fragment: Fragment, block: Snackbar.() -> Unit) {

        val view = fragment.view!!

        Snackbar.make(view, "", Snackbar.LENGTH_SHORT)
            .apply(block)
            .also { snackHolder = SnackHolder(fragment.id, it) }
            .addCallback(object : Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    snackHolder = null
                }
            })
            .show()
    }

    fun hide(fragment: Fragment) {
        snackHolder?.getSnackbar(fragment.id)?.let {
            it.dismiss()
            snackHolder = null
        }
    }

    private class SnackHolder(
        private val fragmentId: Int,
        private val snackbar: Snackbar
    ) {
        fun getSnackbar(id: Int): Snackbar? = when (id) {
            fragmentId -> snackbar
            else -> null
        }
    }
}