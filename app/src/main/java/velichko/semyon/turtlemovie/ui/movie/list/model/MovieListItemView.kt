package velichko.semyon.turtlemovie.ui.movie.list.model

import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.ui.base.adapter.DiffListItemModel

data class MovieListItemView(
    val movie: Movie,
    val onClickAction: (Movie) -> Unit
) : DiffListItemModel {
    override val id: String = movie.title!!
}