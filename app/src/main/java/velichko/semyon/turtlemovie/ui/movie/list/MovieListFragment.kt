package velichko.semyon.turtlemovie.ui.movie.list

import android.content.Intent
import velichko.semyon.turtlemovie.BR
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.ui.base.BaseFragment
import velichko.semyon.turtlemovie.ui.base.extensions.showToast
import velichko.semyon.turtlemovie.ui.movie.list.vm.MovieListViewModel
import velichko.semyon.turtlemovie.ui.movie.list.vm.MovieListViewModelFactory

class MovieListFragment : BaseFragment<MovieListViewModel>(MovieListViewModel::class.java) {

    override val layoutId = R.layout.fragment_movie_list

    override val dataBindingVariable = BR.vmMovieList

    override fun getViewModelFactory() = MovieListViewModelFactory()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        requireContext().showToast(requestCode.toString())

        when (requestCode == RC_SIGN_IN) {
            true -> viewModel.handleActivityResult(resultCode)
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        const val RC_SIGN_IN = 101
    }
}