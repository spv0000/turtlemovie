package velichko.semyon.turtlemovie.ui.movie.comments.model

import velichko.semyon.turtlemovie.data.models.MovieComment
import velichko.semyon.turtlemovie.ui.base.adapter.DiffListItemModel

data class MovieCommentListItemView(
    val movieComment: MovieComment
) : DiffListItemModel {
    override val id: String = movieComment.id!!
}