package velichko.semyon.turtlemovie.ui.base

import androidx.annotation.CallSuper
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import velichko.semyon.turtlemovie.R
import velichko.semyon.turtlemovie.ui.base.buffer.BufferLiveData
import velichko.semyon.turtlemovie.ui.base.event.ViewEvent
import velichko.semyon.turtlemovie.ui.base.event.ViewEventCloseScreen
import velichko.semyon.turtlemovie.ui.base.event.ViewEventShowToast
import kotlin.coroutines.CoroutineContext


abstract class BaseViewModel : ViewModel(), LifecycleObserver, LifecycleOwner, CoroutineScope {

    private val lifecycleRegistry = LifecycleRegistry(this)

    final override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + supervisorJob + exceptionHandler

    val events: LiveData<ViewEvent>
        get() = _events

    init {
        lifecycleRegistry.currentState = Lifecycle.State.STARTED
    }

    protected open val exceptionHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        handleCoroutineException(exception)
    }

    protected open fun handleCoroutineException(exception: Throwable) {
        val resourceProvider = AppDependenciesProvider.resourceProvider
        postViewEvents(ViewEventShowToast.Short(resourceProvider.getString(R.string.error_common)))
    }

    private val _events = BufferLiveData<ViewEvent>()

    private val supervisorJob = SupervisorJob()

    open fun onBackPressed() = postViewEvents(ViewEventCloseScreen())

    protected fun postViewEvents(vararg events: ViewEvent) = events.forEach(_events::setValue)

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED
        supervisorJob.cancel()
    }
}