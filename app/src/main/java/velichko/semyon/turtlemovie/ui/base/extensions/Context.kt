package velichko.semyon.turtlemovie.ui.base.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.getSystemService

fun Context.showToast(text: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show()
}

inline val Context.inputMethodManager: InputMethodManager get() = getSystemService()!!
