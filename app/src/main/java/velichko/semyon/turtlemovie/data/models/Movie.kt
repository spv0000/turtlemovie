package velichko.semyon.turtlemovie.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val title: String?,
    var description: String?,
    val rating: String?,
    var commentList: ArrayList<MovieComment>?
) : Parcelable {
    constructor() : this("", "", "", ArrayList())
}