package velichko.semyon.turtlemovie.data

import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import velichko.semyon.turtlemovie.data.models.Movie
import velichko.semyon.turtlemovie.data.models.MovieComment

class MovieListRepository {

    fun getMovieList(successAction: (List<Movie>) -> Unit, errorAction: () -> Unit) {
        Firebase.database.reference.child(TABLE_MOVIES).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue<List<Movie>>()
                successAction.invoke(value ?: emptyList())
            }

            override fun onCancelled(p0: DatabaseError) {
                errorAction.invoke()
            }
        })
    }

    fun addComment(movie: Movie, comment: String) {

        val amountComments = movie.commentList?.size ?: 0
        val id = amountComments + 1
        val newComment = MovieComment(
            id = id.toString(),
            text = comment,
            authorName = Firebase.auth.currentUser!!.displayName
        )
        if (movie.commentList == null) {
            movie.commentList = ArrayList()
        }
        movie.commentList!!.add(newComment)
        Firebase.database.reference.child(TABLE_MOVIES).orderByChild(TITLE).equalTo(movie.title).addValueEventListener(
            object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (snapshot in dataSnapshot.children) {
                        snapshot.ref.child(COMMENT_LIST).setValue(movie.commentList)
                    }
                }
            }
        )
    }

    fun getMovie(movie: Movie, successAction: (Movie) -> Unit) {
        Firebase.database.reference.child(TABLE_MOVIES).orderByChild(TITLE).equalTo(movie.title).addValueEventListener(
            object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {}
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.getValue<List<Movie>>()
                    successAction.invoke(value?.firstOrNull() ?: movie)
                }
            }
        )
    }

    companion object {
        private const val TABLE_MOVIES = "movies"
        private const val COMMENT_LIST = "commentList"
        private const val TITLE = "title"
    }
}