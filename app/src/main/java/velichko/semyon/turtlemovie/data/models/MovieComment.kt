package velichko.semyon.turtlemovie.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieComment(
    val id: String?,
    val text: String?,
    val authorName: String?
) : Parcelable {
    constructor() : this("", "", "")
}