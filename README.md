# README #

Test project Turtle Movie

### How to run ###

Use apk file that signed with correct key. Firebase Auth doesn't work without this requirement

### Tech stack ###

* Kotlin
* MVVM
* Coroutines
* LiveDate
* Firebase

### Things for improvement ###

* Unit tests
* Design
* Interesting features (e.g. searching of random movie with high rating) 

